package com.tsc.smironova.tm.controller;

import com.tsc.smironova.tm.api.IProjectController;
import com.tsc.smironova.tm.api.IProjectService;
import com.tsc.smironova.tm.model.Project;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.TerminalUtil;

import java.util.List;

public class ProjectController implements IProjectController {

    private final IProjectService projectService;

    public ProjectController(final IProjectService projectService) {
        this.projectService = projectService;
    }

    @Override
    public void showList() {
        System.out.println("[PROJECT LIST]");
        final List<Project> projects = this.projectService.findAll();
        int index = 1;
        for (final Project project : projects) {
            System.out.println(index + ". " + project);
            index++;
        }
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void create() {
        System.out.println("[PROJECT CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Project project = this.projectService.add(name, description);
        if (project == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        this.projectService.clear();
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

}
