package com.tsc.smironova.tm.controller;

import com.tsc.smironova.tm.api.ITaskController;
import com.tsc.smironova.tm.api.ITaskService;
import com.tsc.smironova.tm.model.Task;
import com.tsc.smironova.tm.util.ColorUtil;
import com.tsc.smironova.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(final ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showList() {
        System.out.println("[TASK LIST]");
        final List<Task> tasks = this.taskService.findAll();
        int index = 1;
        for (final Task task : tasks) {
            System.out.println(index + ". " + task);
            index++;
        }
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void create() {
        System.out.println("[TASK CREATE]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        final Task task = this.taskService.add(name, description);
        if (task == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

    @Override
    public void clear() {
        System.out.println("[TASK CLEAR]");
        this.taskService.clear();
        System.out.println(ColorUtil.GREEN + "[OK]" + ColorUtil.RESET);
        System.out.println("--------------------");
    }

}
