package com.tsc.smironova.tm.api;

public interface IProjectController {

    void showList();

    void create();

    void clear();

}
