package com.tsc.smironova.tm.api;

import com.tsc.smironova.tm.model.Task;

import java.util.List;

public interface ITaskRepository {

    void add(Task task);

    void remove(Task task);

    void clear();

    List<Task> findAll();

}
