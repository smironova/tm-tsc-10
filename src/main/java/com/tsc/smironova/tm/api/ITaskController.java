package com.tsc.smironova.tm.api;

public interface ITaskController {

    void showList();

    void create();

    void clear();

}
