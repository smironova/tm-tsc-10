package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.IProjectRepository;
import com.tsc.smironova.tm.model.Project;

import java.util.ArrayList;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> list = new ArrayList<>();

    @Override
    public List<Project> findAll() {
        return this.list;
    }

    @Override
    public void add(final Project project) {
        this.list.add(project);
    }

    @Override
    public void remove(final Project project) {
        this.list.remove(project);
    }

    @Override
    public void clear() {
        this.list.clear();
    }

}
