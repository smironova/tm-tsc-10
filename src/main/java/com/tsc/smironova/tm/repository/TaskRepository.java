package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.ITaskRepository;
import com.tsc.smironova.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> list = new ArrayList<>();

    @Override
    public List<Task> findAll() {
        return this.list;
    }

    @Override
    public void add(final Task task) {
        this.list.add(task);
    }

    @Override
    public void remove(final Task task) {
        this.list.remove(task);
    }

    @Override
    public void clear() {
        this.list.clear();
    }

}
