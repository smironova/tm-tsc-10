package com.tsc.smironova.tm.repository;

import com.tsc.smironova.tm.api.ICommandRepository;
import com.tsc.smironova.tm.constant.ArgumentConst;
import com.tsc.smironova.tm.constant.TerminalConstant;
import com.tsc.smironova.tm.model.Command;

public class CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConstant.ABOUT, ArgumentConst.ARG_ABOUT, "Display developer info."
    );
    private static final Command VERSION = new Command(
            TerminalConstant.VERSION, ArgumentConst.ARG_VERSION, "Display program version."
    );
    private static final Command HELP = new Command(
            TerminalConstant.HELP, ArgumentConst.ARG_HELP, "Display list of terminal commands."
    );
    private static final Command INFO = new Command(
            TerminalConstant.INFO, ArgumentConst.ARG_INFO, "Display system information."
    );
    private static final Command TASK_LIST = new Command(
            TerminalConstant.TASK_LIST, null, " Show task list."
    );
    private static final Command TASK_CREATE = new Command(
            TerminalConstant.TASK_CREATE, null, "Create new task."
    );
    private static final Command TASK_CLEAR = new Command(
            TerminalConstant.TASK_CLEAR, null, "Clear all tasks."
    );
    private static final Command PROJECT_LIST = new Command(
            TerminalConstant.PROJECT_LIST, null, " Show project list."
    );
    private static final Command PROJECT_CREATE = new Command(
            TerminalConstant.PROJECT_CREATE, null, "Create project task."
    );
    private static final Command PROJECT_CLEAR = new Command(
            TerminalConstant.PROJECT_CLEAR, null, "Clear all projects."
    );
    private static final Command COMMANDS = new Command(
            TerminalConstant.COMMANDS, null, "Show program commands."
    );
    private static final Command ARGUMENTS = new Command(
            TerminalConstant.ARGUMENTS, null, "Show program arguments."
    );
    private static final Command EXIT = new Command(
            TerminalConstant.EXIT, null, "Close application."
    );
    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, VERSION, HELP, INFO,
            TASK_LIST, TASK_CREATE, TASK_CLEAR,
            PROJECT_LIST, PROJECT_CREATE, PROJECT_CLEAR,
            COMMANDS, ARGUMENTS, EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
