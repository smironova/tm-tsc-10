package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.ITaskRepository;
import com.tsc.smironova.tm.api.ITaskService;
import com.tsc.smironova.tm.model.Task;

import java.util.List;

public class TaskService implements ITaskService {

    private final ITaskRepository taskRepository;

    public TaskService(final ITaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Override
    public List<Task> findAll() {
        return this.taskRepository.findAll();
    }

    @Override
    public Task add(final String name, final String description) {
        if (name == null || name.isEmpty() || description == null || description.isEmpty())
            return null;
        final Task task = new Task(name, description);
        this.add(task);
        return task;
    }

    @Override
    public void add(final Task task) {
        if (task == null)
            return;
        this.taskRepository.add(task);
    }

    @Override
    public void remove(final Task task) {
        if (task == null)
            return;
        this.taskRepository.remove(task);
    }

    @Override
    public void clear() {
        this.taskRepository.clear();
    }

}
