package com.tsc.smironova.tm.service;

import com.tsc.smironova.tm.api.IProjectRepository;
import com.tsc.smironova.tm.api.IProjectService;
import com.tsc.smironova.tm.model.Project;

import java.util.List;

public class ProjectService implements IProjectService {

    private final IProjectRepository projectRepository;

    public ProjectService(final IProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    @Override
    public List<Project> findAll() {
        return this.projectRepository.findAll();
    }

    @Override
    public Project add(final String name, final String description) {
        if (name == null || name.isEmpty() || description == null || description.isEmpty())
            return null;
        final Project project = new Project(name, description);
        this.add(project);
        return project;
    }

    @Override
    public void add(final Project project) {
        if (project == null)
            return;
        this.projectRepository.add(project);
    }

    @Override
    public void remove(final Project project) {
        if (project == null)
            return;
        this.projectRepository.remove(project);
    }

    @Override
    public void clear() {
        this.projectRepository.clear();
    }

}
